package main

import (
	"encoding/json"
	//"time"
	"gitlab.com/flaneurtv/samm-client-go/src"
	//"sync"
	"fmt"
)

type Tick struct {
	TickUUID      string `json:"tick_uuid"`
	TickTimestamp string `json:"tick_timestamp"`
}

type Alive struct {
	TickReference *Tick `json:"tick_reference"`
}

func handleTick(c *samm.Client, msg *samm.Message) {
	tick := &Tick{}
	err := json.Unmarshal([]byte(*msg.Payload), tick)
	if err != nil {
		c.WriteErrf("Can't marshal response object %s", err)
		return
	}
  alive := &Alive{
		TickReference: tick,
	}
	topic := fmt.Sprintf("alive/%s/%s", c.Config.ServiceName, c.Config.ServiceUUID)
	m := c.NewMessage(topic, alive, "Alive")
	c.EmitMessage(m)
}

func main() {
	c := samm.NewClient()
	c.RegisterTopic("tick", handleTick)
	c.Listen()
}
