# -- Builder Container -- #
FROM golang:1.10 AS builder
COPY ./src/. /go/src/gitlab.com/flaneurtv/samm-tick-responder-go/src/.

RUN cd /go/src/gitlab.com/flaneurtv/samm-tick-responder-go/src && \
    CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build \
    -o /go/bin/processor ./main.go

# -- Runtime Container -- #
FROM alpine:3.8

ENV SERVICE_NAME=tick-responder
WORKDIR /srv/

COPY --from=flaneurtv/samm /usr/local/bin/samm /usr/local/bin/samm
COPY --from=builder /go/bin/processor /srv/processor

COPY ./subscriptions.txt /srv/subscriptions.txt

CMD ["samm"]
