# samm-tick-responder-go #
This project demonstates usage of SAMM - Service Adapter for MQTT Microservices to connect to message bus. It publishes simple response on every tick message it receives.

* SAMM: https://github.com/flaneurtv/samm
  You only have to worry about correctly addressing the JSON line-by-line stdin/out/err interface of SAMM.

# Running samm-tick-responder-go
To run a demo just execute
```
docker-compose build
docker-compose up
```

# Tools
The most simple tool to publish and subscribe to MQTT are the mosquitto_clients:
```
Debian/Ubuntu:
apt-get install mosquitto_clients

MAC OSX:
brew install mosquitto_clients

Both:
To see placeholder test-echo setup running:
mosquitto_sub -t '#'
mosquitto_pub -t 'default/test' -m '{"a":"b"}'
```
Another very feature rich tool is mqtt-spy https://github.com/eclipse/paho.mqtt-spy
